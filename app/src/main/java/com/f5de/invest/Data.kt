package com.f5de.invest

class Data{
    val data: ArrayList<UserStocks> = ArrayList()
    var stockMoney: Float = 0f
    var changeMoney: Float = 0f
    var freeMoney: Float = 0f
    var startMoney: Float = 0f
    var timeStart: Int = 0
}