package com.f5de.invest

import androidx.fragment.app.Fragment

abstract class FragmentEx: Fragment() {
    abstract fun update()
}